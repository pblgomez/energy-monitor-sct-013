#include <Arduino.h>
#include <ArduinoOTA.h>
#include <ESP8266WiFi.h>
#include <PubSubClient.h>
#include "secrets.h"

#include <EmonLib.h>
#include "settings.h"



/////////////////////// FOR DEBUGGING ONLY ////////////////////////////////////
WiFiServer TelnetServer(23);
WiFiClient Telnet;

void handleTelnet(){
  if (TelnetServer.hasClient()) {
    //client is Connected
    if (!Telnet || !Telnet.connected()) {
      if (Telnet) Telnet.stop();    //Cliente desconectado
      Telnet = TelnetServer.available(); //ready for new client
    } else {
      TelnetServer.available().stop();  // have client
    }
  }
}
void TelnetTime(){
  Telnet.println("uptime: " + (String)millis() + " ms");
  delay (2000);
}
//////////////////////////////////////////////////////////////////////////////




// Network setup
WiFiClient espClient;

// MQTT setup
PubSubClient client(espClient);
const PROGMEM char *commandTopic = MQTT_COMMAND_TOPIC;
const PROGMEM char *statusTopic = MQTT_STATE_TOPIC;
void callback(char* topic, byte* payload, unsigned int length){
  

  // Receiving MQTT
  Serial.print((String)"\nRecibiendo mensajes mqtt ["+topic+"] : ");
  for (int i=0; i<length; i++){
    Serial.print((char)payload[i]);
    payload[length] = '\0'; // Make payload a string by NULL terminating it.
    // int integer = atoi((char *)payload);  // Convert string to int  
  }
}





// Crear una instancia EnergyMonitor
EnergyMonitor energyMonitor;



void setup() {
  // Start the Serial communication to send messages to the computer
  Serial.begin(115200);
  delay(10);
  Serial.println('\n');
  
  
  
  // Set Hostname y conectar a wifi
  WiFi.hostname(HOSTNAME);
  WiFi.begin(WIFI_SSID, WIFI_PASSWORD);
  Serial.print("Connecting to ");
  Serial.print(WIFI_SSID); Serial.println(" ...");
  int i = 0;
  while(WiFi.status() != WL_CONNECTED) {
    delay(1000);
    Serial.print(++i); Serial.print(' ');
  }
  Serial.println('\n');
  Serial.println("Connection established!");  
  Serial.print("IP address:\t");
  Serial.println(WiFi.localIP()); // Send the IP address of the ESP8266 to the computer
  Serial.print("hostname:\t");
  Serial.println(WiFi.hostname());




  // 4 lineas para Arduino OTA, 3 aquí y 1 en el loop
  String hostname(HOSTNAME);
  ArduinoOTA.setHostname((const char *)hostname.c_str());
  ArduinoOTA.begin();




  // MQTT
  client.setServer(MQTT_SERVER, 1883);
  client.setCallback(callback);
  ////////////////////// FOR DEBUGGING ONLY ////////////////////////////////////
  TelnetServer.begin();
  TelnetServer.setNoDelay(true);
  // Example of Telnet print
  // Telnet.print("connected to ");
  // Telnet.println(MQTT_SERVER);
  //////////////////////////////////////////////////////////////////////////////



  energyMonitor.current(PIN, CAL);
}




void reconnect() {
 // Loop until we're reconnected to MQTT
 while (!client.connected()) {
 Serial.print("Attempting MQTT connection...");
 // Attempt to connect
 if (client.connect("ESP8266 Client",MQTT_USER,MQTT_PASSWORD)) {
  Serial.print("connected to ");
  Serial.println(MQTT_SERVER);
  // ... and subscribe to topic
  client.subscribe(MQTT_COMMAND_TOPIC);
 } else {
  Serial.print("failed, rc=");
  Serial.print(client.state());
  Serial.println(" try again in 5 seconds");
  // Wait 5 seconds before retrying
  delay(5000);
  }
 }
}


void loop() {

  // 4 lineas para Arduino OTA, 1 aquí y 3 en el loop
  ArduinoOTA.handle();




  ////////////////////// FOR DEBUGGING ONLY ////////////////////////////////////
  handleTelnet();
  TelnetTime();
  //////////////////////////////////////////////////////////////////////////////



  // MQTT Reconnect
  if (!client.connected()) {
    reconnect();
  }
  client.loop();




  // Obtenemos el valor de la corriente eficaz
  // Pasamos el número de muestras que queremos tomar
  double Irms = energyMonitor.calcIrms(MUESTRAS);

  // Calculamos la potencia aparente
  double potencia =  Irms * voltajeRed;

  // Mostramos la información por el monitor serie
  Serial.print("Potencia = ");
  Serial.print(potencia);
  Serial.print("    Irms = ");
  Serial.println(Irms);
  char result[20];
  dtostrf(Irms, 6, 2, result);
  // sprintf(message,potencia);
  client.publish(MQTT_STATE_TOPIC, result);


 }