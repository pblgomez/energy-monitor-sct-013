// Rename to secrets.h


// WIFI
#define WIFI_SSID ""
#define WIFI_PASSWORD ""
#define HOSTNAME "template" // e.g. template.local

// MQTT
#define MQTT_SERVER "192.168.XXX.XXX"
#define MQTT_USER ""
#define MQTT_PASSWORD ""
#define MQTT_COMMAND_TOPIC ""
#define MQTT_STATE_TOPIC ""
