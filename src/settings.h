// Voltaje de nuestra red eléctrica
float voltajeRed = 229.0;
// Iniciamos la clase indicando
// Número de pin: donde tenemos conectado el SCT-013
// Valor de calibración: valor obtenido de la calibración teórica
const int PIN = A0; // Número de pin
const int CAL = 94; // Valor de calibración

// Obtenemos el valor de la corriente eficaz
// Pasamos el número de muestras que queremos tomar
// const int MUESTRAS = 5;
const int MUESTRAS = 1484;
//
